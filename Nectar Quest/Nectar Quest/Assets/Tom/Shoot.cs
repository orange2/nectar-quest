﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Shoot : MonoBehaviour {

	public GameObject Magic;
	public float enemyDamage = 25f;
	public float speed = 10;

	private const float delayTime = 0.5f;
	private float interval  = 0.0f;

	void Update ()	
	{
		if(Input.GetKey(KeyCode.Mouse0))
		{
			if (interval > delayTime) {
				interval = 0.0f;
				return;
			}

			interval += Time.deltaTime;

			GameObject m = (GameObject) Instantiate(Magic, transform.position, transform.rotation);
			m.GetComponent<ParticleSystem> ().Play (true);

			RaycastHit hit;
			Ray ray = new Ray(transform.position, transform.forward);
			if (Physics.Raycast (ray, out hit, 100f)) 
			{
				if (hit.transform.tag == "Enemy") 
				{
				//	hit.transform.GetComponent<ScriptHealth> ().RemoveHealth (enemyDamage);
				}
			}
		}


	}
}