﻿using UnityEngine;
using System.Collections;

public class MagicVelocity : MonoBehaviour {
	float timer = 100;
	float speed = 25;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		timer--;

		transform.position += transform.forward * speed * Time.fixedDeltaTime;

		if (timer <= 0) {
			Destroy (gameObject);
		}

	}

		/*void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.CompareTag("enemy"))
			{
				other.gameObject.SetActive (false);
			}
		}*/
	

}
