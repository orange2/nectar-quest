﻿////////////////////////////////////////////////////////////
// File: PlayerControllerScript.cs
// Author: Abbas Khan and Kyle Tugwell
// Brief: Third Person Controller Script (Old Version)
////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;

public class PlayerControllerScript : MonoBehaviour {

    static Animator anim; //static variable for animation

    // RigidBody Variable for player
    public Rigidbody PlayerRB;
    // Jump realted variables
    private Vector3 Jump = new Vector3(0,3,0);
    private float JumpForce =  1.0f;
    private float JumpForce2 = 30.0f;
    private bool Grounded = false;
    private float GroundCheckDistance = 0.2f;
    // Movement boolean
    private bool Moving = false;

    //wing movement | Kyle Tugwell 01/04/2017 13:30
    public GameObject leftWing;
    public GameObject rightWing;
    public bool WingsForward;
    public bool WingsBackward;
    //==========================================

    // Use this for initialization
    void Start () {

        anim = GetComponent<Animator>(); //get animation component for the character
        anim.SetBool("isIdle", true);
        WingsForward = true;
    }

    // Update is called once per frame
    void Update () {

        Moving = false;

        // Wing movement | Kyle Tugwell 01/04/2017 14:01

        if (WingsForward == true)
        {
            StartCoroutine(Wait());
            leftWing.transform.Rotate(0, 0.5f, 0);
            rightWing.transform.Rotate(0, -0.5f, 0);
        }

        if (WingsBackward == true)
        {
            leftWing.transform.Rotate(0, -0.5f, 0);
            rightWing.transform.Rotate(0, 0.5f, 0);
        }

        //==========================================

        // Check to see if the player is grounded
        CheckGroundStatus();

        if(Grounded == true && Moving == true)
        {
            anim.SetBool("isRunning", true);
            anim.SetBool("isFlying", false);
        }
        if(Grounded == true && Moving == false)
        {
            anim.SetBool("isIdle", true);
            anim.SetBool("isFlying", false);
            anim.SetBool("isJump", false);
            anim.SetBool("isRunningJump", false);
            anim.SetBool("isRunning", false);
        }
        if (Grounded == false && Moving == false)
        {
            anim.SetBool("isIdle", false);
            anim.SetBool("isFlying", true);
            anim.SetBool("isJump", false);
            anim.SetBool("isRunningJump", false);
            anim.SetBool("isRunning", false);
        }

        if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.D))
        {
            anim.SetBool("isRunning", false);
            Moving = false;
        }

        // Controls
        if (Input.GetKey(KeyCode.W))
        {
            this.RotateRelativeToCamera(Vector3.forward, Camera.main);
            PlayerRB.velocity = PlayerRB.rotation * new Vector3(0.0f, 0.0f, 4);
            Moving = true;
            if (anim.GetBool("isRunning") == false)
            {
                anim.SetBool("isRunning", true);
            }
        }
        if (Input.GetKey(KeyCode.A))
        {
            this.RotateRelativeToCamera(Vector3.left, Camera.main);
            PlayerRB.velocity = PlayerRB.rotation * new Vector3(0.0f, 0.0f, 4);
            Moving = true;
            if (anim.GetBool("isRunning") == false)
            {
                anim.SetBool("isRunning", true);
            }
        }
        if (Input.GetKey(KeyCode.S))
        {
            this.RotateRelativeToCamera(Vector3.back, Camera.main);
            PlayerRB.velocity = PlayerRB.rotation * new Vector3(0.0f, 0.0f, 4);
            Moving = true;
            if (anim.GetBool("isRunning") == false)
            {
                anim.SetBool("isRunning", true);
            }
        }
        if (Input.GetKey(KeyCode.D))
        {
            this.RotateRelativeToCamera(Vector3.right, Camera.main);
            PlayerRB.velocity = PlayerRB.rotation * new Vector3(0.0f, 0.0f, 4);
            Moving = true;
            if (anim.GetBool("isRunning") == false)
            {
                anim.SetBool("isRunning", true);
            }
        }

        // Jump and Fly
        if (Input.GetKeyDown(KeyCode.Space))
        {
//            anim.SetBool("isRunning", true);
            if (Grounded == true)
            {
                anim.SetBool("isJump", true);

                // Stationary Jump
                if (anim.GetBool("isRunning") == false)
                {
                    anim.SetBool("isJump", true);
                    PlayerRB.AddForce(Jump * (JumpForce), ForceMode.Impulse);
                }
                else if (anim.GetBool("isRunning") == true)
                {
                    anim.SetBool("isFlying", false);
                    anim.SetBool("isJump", true);
                    PlayerRB.AddForce(Jump * (JumpForce2), ForceMode.Impulse);
                }
                
//                }
//               else
//                {
//                    anim.SetBool("isRunningJump", true);
//                   // Running Jump
//                    PlayerRB.AddForce(Jump * (JumpForce * 20), ForceMode.Impulse);
//                    anim.SetBool("isFlying", false);
//                    anim.SetBool("isJump", true);
//                    anim.SetBool("isRunning", true);
//                }
            }
            else if (Grounded == false)
            {
                    // Fly
                    anim.SetBool("isFlying", true);
                    PlayerRB.AddForce(Jump * JumpForce, ForceMode.Impulse);
                
            }
        }

    }

    private void RotateRelativeToCamera(Vector3 Direction, Camera Cam)
    {
        // Get the camera's rotation
        Vector3 CamDir = Cam.transform.rotation * Direction;

        // Add the camera's direction to the player's direction
        Vector3 PlayerDir = transform.position + CamDir;

        // Quaternion for direction
        Quaternion targetRotation = Quaternion.LookRotation(PlayerDir - transform.position);

        // Lock the Y-Axis so player doesn't fall over
        Quaternion constrained = Quaternion.Euler(0.0f, targetRotation.eulerAngles.y, 0.0f);

        // Smooth out the rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, constrained, Time.deltaTime * 10);
    }

    void CheckGroundStatus()
    {
        // Create a RayCast
        RaycastHit hitInfo;
        // For Testing
        Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * GroundCheckDistance));
        // If the ray hits the floor then the player is on the ground
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, GroundCheckDistance))
        {
            Grounded = true;
        }
        else
        {
            Grounded = false;
        }
    }

    // Wing movement | Kyle Tugwell 01/04/2017 14:51
    IEnumerator Wait()
    {
        WingsBackward = false;
        WingsForward = true;
        yield return new WaitForSeconds(1.0f);
        WingsForward = false;
        WingsBackward = true;
        yield return new WaitForSeconds(0.5f);
        WingsBackward = false;
        WingsForward = true;
    }
    //==========================================

}
