﻿////////////////////////////////////////////////////////////
// File: PlayerControllerScriptRevamp.cs
// Author: Abbas Khan
// Brief: Third Person Controller Script
////////////////////////////////////////////////////////////

// Using the UntyEngine Directory
using UnityEngine;

public class PlayerControllerScriptRevamp : MonoBehaviour
{
    // Static Animator Variable used to perform the player animations
    static Animator PlayerAnimator;
    // RigidBody Variable for player
    public Rigidbody PlayerRB;
    // Boolean used to tell if the player is grounded
    private bool Grounded;
    // Float that stores the distance for the raycast to be fired (See the CheckGroundStatus function)
    private float GroundCheckDistance;

    // Jump realted variables
    private Vector3 Jump = new Vector3(0, 3, 0);
    private float StationaryJumpForce = 1.5f;
    private float RunningJumpForce = 2.5f;
    // Boolean used to tell if the player is moving or not
    private bool Flying;
    // Boolean used to tell if the player is moving or not
    private bool Jumped;
    // Boolean used to tell if the player is moving or not
    private bool Moving;
    // Star called at the begining
    void Start()
    {
        // Get the animator component from the character
        PlayerAnimator = GetComponent<Animator>();
        // Set grounded to true
        Grounded = true;
        // Set moving to false
        Moving = false;
        // Set moving to false
        Jumped = false;
        // Set Flying to false
        Flying = false;
        // Set the distance to 0.2
        GroundCheckDistance = 0.2f;
    }
    // Update called once per frame
    void Update()
    {
        // Check to see if the player is grounded
        CheckGroundStatus();
        // Check to see if the player is moving
        CheckMovementStatus();
        // Animator Setup
        // If the player is grounded and not moving
        if (Grounded == true && Moving == false)
        {
            PlayerAnimator.SetBool("isIdle", true);
            PlayerAnimator.SetBool("isFlying", false);
            PlayerAnimator.SetBool("isJump", false);
            PlayerAnimator.SetBool("isRunning", false);
        }
        // If the player is grounded and moving
        else if (Grounded == true && Moving == true)
        {
            PlayerAnimator.SetBool("isIdle", false);
            PlayerAnimator.SetBool("isFlying", false);
            PlayerAnimator.SetBool("isJump", false);
            PlayerAnimator.SetBool("isRunning", true);
        }
        // If the player is grounded
        if (Grounded == true)
        {
            // Stationary Jump (If space is pressed and the player is not moving)
            if (Input.GetKeyDown(KeyCode.Space) && Moving == false)
            {
                // Set jumped to true
                // Start the animation
                // Add the force
                Jumped = true;
                PlayerAnimator.SetBool("isJump", true);
                PlayerRB.AddForce(Jump * (StationaryJumpForce), ForceMode.Impulse);
            }
            // Running Jump (If space is pressed and the player is moving)
            if (Input.GetKeyDown(KeyCode.Space) && Moving == true)
            {
                // Set jumped to true
                // Start the animation
                // Add the forces
                Jumped = true;
                PlayerAnimator.SetBool("isJump", true);
                PlayerRB.AddForce(Jump * (RunningJumpForce), ForceMode.Impulse);
            }
            // Controls
            // If W is pressed and jumped is false
            if (Input.GetKey(KeyCode.W) && Jumped == false)
            {
                // Set moving to true
                // Start the animation
                // Rotate the player to the camera's direction
                // Add the force
                Moving = true;
                PlayerAnimator.SetBool("isRunning", true);
                this.RotateRelativeToCamera(Vector3.forward, Camera.main);
                PlayerRB.velocity = PlayerRB.rotation * new Vector3(0.0f, 0.0f, 4);
            }
            // If a is pressed and jumped is false
            if (Input.GetKey(KeyCode.A) && Jumped == false)
            {
                // Set moving to true
                // Start the animation
                // Rotate the player to the camera's direction
                // Add the force
                Moving = true;
                PlayerAnimator.SetBool("isRunning", true);
                this.RotateRelativeToCamera(Vector3.left, Camera.main);
                PlayerRB.velocity = PlayerRB.rotation * new Vector3(0.0f, 0.0f, 4);
            }
            // If s is pressed and jumped is false
            if (Input.GetKey(KeyCode.S) && Jumped == false)
            {
                // Set moving to true
                // Start the animation
                // Rotate the player to the camera's direction
                // Add the force
                Moving = true;
                PlayerAnimator.SetBool("isRunning", true);
                this.RotateRelativeToCamera(Vector3.back, Camera.main);
                PlayerRB.velocity = PlayerRB.rotation * new Vector3(0.0f, 0.0f, 4);
            }
            // If d is pressed and jumped is false
            if (Input.GetKey(KeyCode.D) && Jumped == false)
            {
                // Set moving to true
                // Start the animation
                // Rotate the player to the camera's direction
                // Add the force
                Moving = true;
                PlayerAnimator.SetBool("isRunning", true);
                this.RotateRelativeToCamera(Vector3.right, Camera.main);
                PlayerRB.velocity = PlayerRB.rotation * new Vector3(0.0f, 0.0f, 4);
            }
        }
        // Airborne Controls
        // If the grounded is false
        else if (Grounded == false)
        {
            // If left shit is pressed and moving is true
            if (Input.GetKeyDown(KeyCode.LeftShift) && Moving == true)
            {
                // Set flying to true and play the animation
                Flying = true;
                PlayerAnimator.SetBool("isFlying", true);
            }
            // If w is pressed and flying is true
            if (Input.GetKey(KeyCode.W) && Flying == true)
            {
                // Set moving to true
                // Start the animation
                // Rotate the player to the camera's direction
                // Add the force
                Moving = true;
                PlayerAnimator.SetBool("isFlying", true);
                this.RotateRelativeToCamera(Vector3.forward, Camera.main);
                PlayerRB.velocity = PlayerRB.rotation * new Vector3(0.0f, 0.0f, 5);
            }
            // If a is pressed and flying is true
            if (Input.GetKey(KeyCode.A) && Flying == true)
            {
                // Set moving to true
                // Start the animation
                // Rotate the player to the camera's direction
                // Add the force
                Moving = true;
                PlayerAnimator.SetBool("isFlying", true);
                this.RotateRelativeToCamera(Vector3.left, Camera.main);
                PlayerRB.velocity = PlayerRB.rotation * new Vector3(0.0f, 0.0f, 5);
            }
            // If s is pressed and flying is true
            if (Input.GetKey(KeyCode.S) && Flying == true)
            {
                // Set moving to true
                // Start the animation
                // Rotate the player to the camera's direction
                // Add the force
                Moving = true;
                PlayerAnimator.SetBool("isFlying", true);
                this.RotateRelativeToCamera(Vector3.back, Camera.main);
                PlayerRB.velocity = PlayerRB.rotation * new Vector3(0.0f, 0.0f, 5);
            }
            // If d is pressed and flying is true
            if (Input.GetKey(KeyCode.D) && Flying == true)
            {
                // Set moving to true
                // Start the animation
                // Rotate the player to the camera's direction
                // Add the force
                Moving = true;
                PlayerAnimator.SetBool("isFlying", true);
                this.RotateRelativeToCamera(Vector3.right, Camera.main);
                PlayerRB.velocity = PlayerRB.rotation * new Vector3(0.0f, 0.0f, 5);
            }
        }
    }
    // Function for checking if the player is moving
    void CheckMovementStatus()
    {
        // If non of the keys are pressed
        if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.D))
        {
            // The player is not moving
            Moving = false;
        }
    }
    // Function for checking if the player is grounded
    void CheckGroundStatus()
    {
        // Create a RayCast
        RaycastHit hitInfo;
        // For Testing
        Debug.DrawLine(transform.position + (Vector3.up * 0.18f), transform.position + (Vector3.up * 0.2f) + (Vector3.down * GroundCheckDistance));
        // If the ray hits the floor then the player is on the ground
        if (Physics.Raycast(transform.position + (Vector3.up * 0.18f), Vector3.down, out hitInfo, GroundCheckDistance))
        {
            // Set grounded to true
            // Set flying to false
            // Set jumped to false
            Grounded = true;
            Flying = false;
            Jumped = false;
        }
        // Otherwisee
        else
        {
            // Grounded is false
            Grounded = false;
        }
    }
    // Function for rotating the player's direction to the camera's direction
    private void RotateRelativeToCamera(Vector3 Direction, Camera Cam)
    {
        // Get the camera's rotation
        Vector3 CamDir = Cam.transform.rotation * Direction;

        // Add the camera's direction to the player's direction
        Vector3 PlayerDir = transform.position + CamDir;

        // Quaternion for direction
        Quaternion targetRotation = Quaternion.LookRotation(PlayerDir - transform.position);

        // Lock the Y-Axis so player doesn't fall over
        Quaternion constrained = Quaternion.Euler(0.0f, targetRotation.eulerAngles.y, 0.0f);

        // Smooth out the rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, constrained, Time.deltaTime * 10);
    }
}

