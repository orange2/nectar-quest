﻿////////////////////////////////////////////////////////////
// File: Trigger.cs
// Author: Abbas Khan
// Brief: Used with the Item Collection Puzzle System
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory

public class Trigger : MonoBehaviour {

    // Public gameobject to hold the door
    public GameObject Door;
    // Public gameobject to hold the Item Collection Manager
    public GameObject ICM;
    // When an object enters the trigger area
    void OnTriggerEnter(Collider col)
    {
        // If it's the player
        if(col.gameObject.tag == "Player")
        {
            // Destroy all the components of the ICM system
            Destroy(ICM);
            Destroy(this.gameObject);
            Destroy(Door);
        }
    }
	
}
