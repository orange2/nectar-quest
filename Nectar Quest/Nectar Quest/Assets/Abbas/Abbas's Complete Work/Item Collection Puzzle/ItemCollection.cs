﻿////////////////////////////////////////////////////////////
// File: ItemCollection.cs
// Author: Abbas Khan
// Brief: Used with the Item Collection System
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory

public class ItemCollection : MonoBehaviour {

    // Public reference to the itemcollectionmanager script
    public ItemCollectionPuzzleManager Other;
    // When an object enters the trigger area
    void OnTriggerEnter(Collider Col)
    {
        // If it's the player
        if(Col.gameObject.tag == "Player")
        {
            // Add 1 to the current amount in the manager script by calling AddAmount
            Other.AddAmount();
            // Destroy the item
            Destroy(this.gameObject);
        }
    }
}
