﻿////////////////////////////////////////////////////////////
// File: ItemCollectionPuzzleManager.cs
// Author: Abbas Khan
// Brief: Main script for the item collection system
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory

public class ItemCollectionPuzzleManager : MonoBehaviour {
    // Public int for the target amount for the amount of objects to be collected
    public int TargetAmount;
    // Public int for amount of collected objects
    public int CurrentAmount;
    // Public gameobject to reference the trigger
    public GameObject Trigger;
	// Start function that runs at the start of the script's lifetime
	void Start () {
        // Set the current amount to 0
        CurrentAmount = 0;
	}
	// Update function that is called once per frame
	void Update () {
        // If the current amount is the same as the target amount
	    if(CurrentAmount == TargetAmount)
        {
            // Make the trigger active
            Trigger.SetActive(true);
        } 
	}
    // Add Amount function that increases the amount of collected items int by 1
    public void AddAmount()
    {
        // Increase the vurrent amount by 1
        CurrentAmount = CurrentAmount + 1;
    }
}
