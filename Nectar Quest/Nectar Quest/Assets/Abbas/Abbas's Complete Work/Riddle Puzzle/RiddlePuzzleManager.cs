﻿////////////////////////////////////////////////////////////
// File: RiddlePuzzleManager.cs
// Author: Abbas Khan
// Brief: Eiddle Puzzle System
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory
using UnityEngine.UI;
// Using the UnityEngine UI Directory

public class RiddlePuzzleManager : MonoBehaviour {

    // Public gameobject to hold the riddle panel
    public GameObject RiddlePanel;
    // Public text for the riddle string to go into
    public Text Riddle;
    // Public text for the answer string to go into
    public Text Answer1;
    // Public text for the answer string to go into
    public Text Answer2;
    // Public text for the answer string to go into
    public Text Answer3;
    // Public text for the answer string to go into
    public Text Answer4;
    // Public string that goes into the text object
    public string RiddleText;
    // Public string that goes into the text object
    public string Answer1Text;
    // Public string that goes into the text object
    public string Answer2Text;
    // Public string that goes into the text object
    public string Answer3Text;
    // Public string that goes into the text object
    public string Answer4Text;
    // Public gameobject for the snail or item blocking the path
    public GameObject Snail;
    // When the object enters the trigger
    void OnTriggerEnter(Collider col)
    {
        // If it's the player
        if (col.gameObject.tag == "Player")
        {
            // Set the cursor visible
            Cursor.visible = true;
            // Unlock the mouse
            Cursor.lockState = CursorLockMode.None;
            // Set the panel to active
            RiddlePanel.SetActive(true);
        }
    }
    // When the player leaves the area
    void OnTriggerExit(Collider col)
    {
        // Set the cursor invisible
        Cursor.visible = false;
        // Lock the mouse
        Cursor.lockState = CursorLockMode.Locked;
        // Hide the riddle
        RiddlePanel.SetActive(false);
    }
	// Start function which runs at the start of the script's lifetime
	void Start ()
    {
        // Set the text objects text equal to the text in the strings
        Riddle.text = RiddleText;
        Answer1.text = Answer1Text;
        Answer2.text = Answer2Text;
        Answer3.text = Answer3Text;
        Answer4.text = Answer4Text;
	}
	// Correct Answer function that operates when the correct button is clicked
    public void CorrectAnswer()
    {
        // Make the cursor invisible
        Cursor.visible = false;
        // Lock the mouse
        Cursor.lockState = CursorLockMode.Locked;
        // Delete the all the riddle related objects
        Destroy(Snail);
        Destroy(Riddle);
        Destroy(Answer1);
        Destroy(Answer2);
        Destroy(Answer3);
        Destroy(Answer4);
        Destroy(RiddlePanel);
        Destroy(this.gameObject);
    }
    // Incorrect Answer function that operates when the incorrect button is clicked
    public void IncorrectAnswer()
    {
        // Hide the riddle
        RiddlePanel.SetActive(false);
        // Make it invisible
        Cursor.visible = false;
        // Lock the mouse
        Cursor.lockState = CursorLockMode.Locked;
    }
}
