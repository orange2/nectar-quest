﻿////////////////////////////////////////////////////////////
// File: RADManager.cs (RAD = Research and Development)
// Author: Abbas Khan
// Brief: Upgrade System
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory
using UnityEngine.UI;
// Using the UnityEngine UI Directory
using System.Collections;
// Using the Systems Collections Directory

public class RADManager : MonoBehaviour {

    // Private int variable used to hold the Health Upgrade Level
    private int HealthUpgradeLevel = 0;
    // Private int variable used to hold the stamina Upgrade Level
    private int StaminaUpgradeLevel = 0;
    // Private int variable used to hold the mana Upgrade Level
    private int ManaUpgradeLevel = 0;
    // Private int variable used to hold the amount of nectar (Currency)
    private int Nectar;
    // Public text variable used to hold the text where the amount of nectar is displayed
    public Text NectarCount;
    // Public GameObject array that is used to hold the Upgraded UI Images
    public GameObject[] UpgradedImages;
    // Public GameObject array that is used to hold the Upgraded UI Images
    public GameObject[] UpgradedImages1;
    // Public GameObject array that is used to hold the Upgraded UI Images
    public GameObject[] UpgradedImages2;
    // Public GameObject variable used to hold the UI Image which is displayed if the player doesn't have enough points for an upgrade
    public GameObject NotEnoughPoints;
    // Public gameobject that is used to reference the shop UI panel
    public GameObject ShopPanel;
    // Public gameobject that is used to reference the shop UI text
    public GameObject ShopText;
    // Boolean variable that determines whether the player is on the shop panel or not
    private bool InArea;
    // Public gameobject used to reference the pause menu manager
    public GameObject PauseMenuManager;
    // When the player enter's the trigger
    void OnTriggerEnter(Collider Col)
    {
        // If the tag is player
        if (Col.gameObject.tag == "Player")
        {
            // Activate the pop up text
            ShopText.SetActive(true);
            // Set the bool to true
            InArea = true;
        }
    }
    // When the object leaves the trigger area
    void OnTriggerExit(Collider Col)
    {
        // Set the bool to false
        InArea = false;
        // Deactivate the text
        ShopText.SetActive(false);
    }

    // Ienumerator used to temporarily display the Not Enough Points UI Image (Takes a float variable as the parameter for how long the wait is
    IEnumerator ShowUI(float WaitTime)
    {
        // Set the gamobject to active which makes the image visible
        NotEnoughPoints.SetActive(true);
        // Wait for the amount of seconds passed in by the paramter
        yield return new WaitForSecondsRealtime(WaitTime);
        // Set the gameobject to not acvtive which hides the image
        NotEnoughPoints.SetActive(false);
    } 
    // Start function which operates at the start of the script
    void Start () {
        // Add 100 to the amount, this way it's easier to demo
        PlayerData.Instance.Amountofcoin = 100;
        // Set the health upgrade level equal to the stored value
        HealthUpgradeLevel = PlayerPrefs.GetInt("HealthUpgrade");
        // Set the stamina upgrade level equal to the stored value
        StaminaUpgradeLevel = PlayerPrefs.GetInt("StaminaUpgrade");
        // Set the mana upgrade level equal to the stored value
        ManaUpgradeLevel = PlayerPrefs.GetInt("ManaUpgrade");
        // Call the update shop ui function and pass in the upgrade level and the image by reference
        UpdateShopUI(ref ManaUpgradeLevel, ref UpgradedImages);
        // Call the update shop ui function and pass in the upgrade level and the image by reference
        UpdateShopUI(ref StaminaUpgradeLevel, ref UpgradedImages1);
        // Call the update shop ui function and pass in the upgrade level and the image by reference
        UpdateShopUI(ref HealthUpgradeLevel, ref UpgradedImages2);
    }
	// Update function that is called once per frame
	void Update () {
        // Set the nectar integer equal to the stored value
        Nectar = PlayerData.Instance.Amountofcoin;
        // Set the text ui equal to the nectar amount (Currency)
        NectarCount.text = Nectar.ToString();
        // Set the object to true
        PauseMenuManager.SetActive(true);
        // If in area is true
        if(InArea == true)
        {
            // Set the object to false
            PauseMenuManager.SetActive(false);
            // if F is pressed
            if (Input.GetKeyDown(KeyCode.F))
            {
                // Activate the panel
                ShopPanel.SetActive(true);
                // Freaaze Time
                Time.timeScale = 0;
                // Unlock the mouse cursor
                Cursor.lockState = CursorLockMode.None;
                // Set it to visible
                Cursor.visible = true;
            }
            // if escape is pressed
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                // Deactivate the panel
                ShopPanel.SetActive(false);
                // Unfreaaze Time
                Time.timeScale = 1;
                // lock the mouse cursor
                Cursor.lockState = CursorLockMode.Locked;
                // Set it to not visible
                Cursor.visible = false;
            }
        }
    }
    // Purchase health upgrade function
    public void PurchaseHealthUpgrade()
    {
        // If the upgrade level is less than 5
        if (HealthUpgradeLevel < 5)
        {
            // If the nectar amount (Currency) is more than or equal to 10
            if (Nectar >= 10)
            {
                // Increase the upgrade level by 1
                HealthUpgradeLevel = HealthUpgradeLevel + 1;
                // Decrease the amount of coin 10
                PlayerData.Instance.Amountofcoin = PlayerData.Instance.Amountofcoin - 10;
                // Set the stored value equal to the local value
                PlayerPrefs.SetInt("HealthUpgrade", HealthUpgradeLevel);
                // Save the data
                PlayerPrefs.Save();
                PlayerData.Instance.SaveAmountofcoin();
                // Update the shop UI
                UpdateShopUI(ref HealthUpgradeLevel, ref UpgradedImages);
            }
            // Otherwise
            else
            {
                // Call the show UI enumerator and pass in the float variable
                StartCoroutine(ShowUI(1.0f));
            }
        }
    }
    // Purchase stamina upgrade function
    public void PurchaseStaminaUpgrade()
    {
        // If the upgrade level is less than 5
        if (StaminaUpgradeLevel < 5)
        {
            // If the nectar amount (Currency) is more than or equal to 10
            if (Nectar >= 10)
            {
                // Increase the upgrade level by 1
                StaminaUpgradeLevel = StaminaUpgradeLevel + 1;
                // Decrease the amount of coin 10
                PlayerData.Instance.Amountofcoin = PlayerData.Instance.Amountofcoin - 10;
                // Set the stored value equal to the local value
                PlayerPrefs.SetInt("StaminaUpgrade", StaminaUpgradeLevel);
                // Save the data
                PlayerPrefs.Save();
                PlayerData.Instance.SaveAmountofcoin();
                // Update the shop UI
                UpdateShopUI(ref StaminaUpgradeLevel, ref UpgradedImages1);
            }
            // Otherwise
            else
            {
                // Call the show UI enumerator and pass in the float variable
                StartCoroutine(ShowUI(1.0f));
            }
        }
    }
    // Purchase mana upgrade function
    public void PurchaseManaUpgrade()
    {
        // If the upgrade level is less than 5
        if (ManaUpgradeLevel < 5)
        {
            // If the nectar amount (Currency) is more than or equal to 10
            if (Nectar >= 10)
            {
                // Increase the upgrade level by 1
                ManaUpgradeLevel = ManaUpgradeLevel + 1;
                // Decrease the amount of coin 10
                PlayerData.Instance.Amountofcoin = PlayerData.Instance.Amountofcoin - 10;
                // Set the stored value equal to the local value
                PlayerPrefs.SetInt("ManaUpgrade", ManaUpgradeLevel);
                // Save the data
                PlayerPrefs.Save();
                PlayerData.Instance.SaveAmountofcoin();
                // Update the shop UI
                UpdateShopUI(ref ManaUpgradeLevel, ref UpgradedImages2);
            }
            // Otherwise
            else
            {
                // Call the show UI enumerator and pass in the float variable
                StartCoroutine(ShowUI(1.0f));
            }
        }
    }
    // Update shop UI function that taes two parameters by reference (The upgrade level and the ui images)
    private void UpdateShopUI(ref int UpgradeLevel, ref GameObject[] UpgradeUiImages)
    {
        // Case Switch statement
        // Depednig on the value of the passed in upgrade level
        switch (UpgradeLevel)
        {
            // If it's 0
            case 0:
                // Do nothing
                break;
            // If it's 1
            case 1:
                // Display one of the upgrade unlocked images
                UpgradeUiImages[0].SetActive(true);
                break;
            // If it's 2
            case 2:
                // Display two of the upgrade unlocked images
                UpgradeUiImages[0].SetActive(true);
                UpgradeUiImages[1].SetActive(true);
                break;
            // If it's 3
            case 3:
                // Display three of the upgrade unlocked images
                UpgradeUiImages[0].SetActive(true);
                UpgradeUiImages[1].SetActive(true);
                UpgradeUiImages[2].SetActive(true);
                break;
            // If it's 4
            case 4:
                // Display four of the upgrade unlocked images
                UpgradeUiImages[0].SetActive(true);
                UpgradeUiImages[1].SetActive(true);
                UpgradeUiImages[2].SetActive(true);
                UpgradeUiImages[3].SetActive(true);
                break;
            // If it's 5
            case 5:
                // Display five of the upgrade unlocked images
                UpgradeUiImages[0].SetActive(true);
                UpgradeUiImages[1].SetActive(true);
                UpgradeUiImages[2].SetActive(true);
                UpgradeUiImages[3].SetActive(true);
                UpgradeUiImages[4].SetActive(true);
                break;
                // Default Case
            default:
                break;
        }
    }

}
