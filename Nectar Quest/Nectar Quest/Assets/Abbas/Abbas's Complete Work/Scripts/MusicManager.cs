﻿////////////////////////////////////////////////////////////
// File: MusicManager.cs
// Author: Abbas Khan
// Brief: Music Manager
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory
public class MusicManager : MonoBehaviour {
    // Mute Sound Function
    public void MuteSound()
    {
        // Set the scene's audio listener's volume to 0
        AudioListener.volume = 0;
    }
    // Unmute sound function
    public void UnMuteSound()
    {
        // Set the scene's audio listener's volume to 1
        AudioListener.volume = 1;
    }
}
