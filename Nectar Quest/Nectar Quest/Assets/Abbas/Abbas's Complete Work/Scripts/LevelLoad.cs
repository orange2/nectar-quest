﻿////////////////////////////////////////////////////////////
// File: LevelLoad.cs
// Author: Abbas Khan
// Brief: Used for loading between levels
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory
using UnityEngine.SceneManagement;
// Using the UnityEngine scenemanagement Directory

public class LevelLoad : MonoBehaviour {

    // Integer Variable used to hold the value of the next level to load
    public int LevelToLoad;

    // When an object enters the trigger area
	void OnTriggerEnter(Collider Col)
    {
        // If it's the player
        if (Col.gameObject.tag == "Player")
        {
            // Load the next scene
            SceneManager.LoadScene(LevelToLoad);
        }
    }
}
