﻿////////////////////////////////////////////////////////////
// File: PlayerData.cs
// Author: Abbas Khan
// Brief: Used with the upgrade system
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory

public class PlayerData{
	private static PlayerData _instance;
    // private static instance variable
	public static PlayerData Instance {
		get {
			if (_instance == null) {
				_instance = new PlayerData();
			}
			return _instance;
		}
	}
    // public static instance variable that if it doesn't exist already is created
	public int Amountofcoin;
    // Public int variable for holding the stored amount of currency
    // Function to retrieve the amount of coin from storage
	public void GETCOIN(){
        // Set the int variable equal to the stored valeu
		Amountofcoin = PlayerPrefs.GetInt("COIN");
	}
    // Add currency function
	public void ADDCOIN(){
        // Increase the amount of coin by 10
		Amountofcoin = Amountofcoin + 10;
	}
    // Save the amount of coin to storage
	public void SaveAmountofcoin(){
        // Set the stored value equal to the amount of coin
		PlayerPrefs.SetInt("COIN", Amountofcoin);
        // save it
		PlayerPrefs.Save();
	}
    // Reset all ddata function
	public void ResetAllData(){
        // Delete all the stored values
		PlayerPrefs.DeleteAll();
        // Set the amount of coin to 0
		Amountofcoin = 0;
        // Save the new empty values
		PlayerPrefs.Save();
	}
}
