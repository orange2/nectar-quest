﻿////////////////////////////////////////////////////////////
// File: InGameMenuManager.cs
// Author: Abbas Khan
// Brief: Used for the in game UI
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory
using UnityEngine.SceneManagement;
// Using the UnityEngine scenemanagement Directory

public class InGameMenuManager : MonoBehaviour {

    // Public gameobject pausemenu used to reference the Pause Menu Ui
    public GameObject PauseMenu;

	// Start function that unfreazes time
	void Start ()
    {
        // Unfreaze time
		Time.timeScale = 1;
	}
	// Update function that is called once per frame
	void Update ()
    {
        // If the key is pressed
		if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Make the pause menu active
            PauseMenu.SetActive(true);
            // Unlock the mouse cursor
            Cursor.lockState = CursorLockMode.None;
            // Set it to visible
            Cursor.visible = true;
            // freaze time
            Time.timeScale = 0;
        }
    }
    // resume the game functrion
    public void Resume()
    {
        // Unfreaze time
        Time.timeScale = 1;
        // lock the mouse cursor
        Cursor.lockState = CursorLockMode.Locked;
        // Set it to not visible
        Cursor.visible = false;
    }
    // Restart level function
	public void Restart()
    {
        // Scene variable that holds the current scene
        Scene CurrentScene = SceneManager.GetActiveScene();
        // Load the current scene again
        SceneManager.LoadScene(CurrentScene.name);
	}
    // Mute sound function
	public void MuteSound2()
    {
        // Set the audio listener's volume to 0
		AudioListener.volume = 0;
	}
    // Unmute sound function
    public void UnMuteSound2()
    {
        // Set the audio listener's volume to 1
        AudioListener.volume = 1;
	}
    // Quit game function
    public void QuitGame()
    {
        // Quit the application
        Application.Quit();
    }
}
