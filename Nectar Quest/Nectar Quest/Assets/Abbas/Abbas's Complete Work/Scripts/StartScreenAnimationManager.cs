﻿////////////////////////////////////////////////////////////
// File: StartScreenAnimationManager.cs
// Author: Abbas Khan
// Brief: Script used to work with the start section
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory
using System.Collections;
// Using the Systems Collections Directory

public class StartScreenAnimationManager : MonoBehaviour {

    // Public GameObject variable used to hold the initial UI Panel
    public GameObject DevelopedByPanel;
    // Public GameObject variable used to hold the start UI Panel
    public GameObject StartPanel;
    // Ienumerator used as a wait function to allow the animation to complete and then display the start panel
    IEnumerator StartScreenAnimations()
    {
        // Set the initial ui panel to active which will activate the animation
        DevelopedByPanel.SetActive(true);
        // Wait for the animation to complete
        yield return new WaitForSeconds(3.0f);
        // Set the initial panel to not active which will hide the ui
        DevelopedByPanel.SetActive(false);
        // Set the start panel to active which allow the user to interact with the buttons
        StartPanel.SetActive(true);
    }
    // Start function that operates when the script is activated
    void Start () {
        // Start the coroutine
        StartCoroutine(StartScreenAnimations());
	}
}
