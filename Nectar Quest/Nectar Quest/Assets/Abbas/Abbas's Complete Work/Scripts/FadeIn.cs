﻿////////////////////////////////////////////////////////////
// File: FadeIn.cs
// Author: Abbas Khan
// Brief: Used for fading in for the cutscene
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory
using System.Collections;
// Using the System Collection Directory
using UnityEngine.SceneManagement;
// Using the UnityEngine Scenemanagement Directory

public class FadeIn : MonoBehaviour {

    // Public fade in image which has animation on it
    public GameObject FadeInImage;
    // Public fade out image which has animation on it
    public GameObject FadeOutImage;
    // Ienumerator for the fading
    IEnumerator FadeInWait()
    {
        // Wait for 0.5 seconds
        yield return new WaitForSeconds(0.5f);
        // Set the fade in image to false
        FadeInImage.SetActive(false);
        // Wait for 3 seconds
        yield return new WaitForSeconds(3.0f);
        // Set the fade out image to true
        FadeOutImage.SetActive(true);
        // Wait for 1 second
        yield return new WaitForSeconds(1.0f);
        // Load the first level
        SceneManager.LoadScene(3);
    }

	// Start function called at the start of the script's lifetime
	void Start () {
        // Start the coroutine
        StartCoroutine(FadeInWait());
	}
}
