﻿////////////////////////////////////////////////////////////
// File: PickupScript.cs
// Author: Abbas Khan
// Brief: Used for the gem pickup
////////////////////////////////////////////////////////////

// Using the UnityEngine Directory
using UnityEngine;

public class PickupScript : MonoBehaviour {
    // Public transform for the positioning of the gem
    public Transform Target;
    // public vector 3 for the offset position
    public Vector3 Offset;
    // If the object enters the trigger area
    void OnTriggerEnter(Collider Col)
    {
        // If it's the player
        if (Col.tag == "Player")
        {
            // Add to the currency amount
            PlayerData.Instance.ADDCOIN();
            // Destroy the gem
            Destroy(gameObject);
        }
    }
	// Update function is called once per frame
	void Update () {
        // Set the gem's position
        gameObject.transform.position = Target.transform.position + Offset;
	}
}
