﻿////////////////////////////////////////////////////////////
// File: END.cs
// Author: Abbas Khan
// Brief: Used for ending the mid-term demo version
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory
using UnityEngine.SceneManagement;
// Using the UnityEngine scenemanagement Directory

public class END : MonoBehaviour {
    // When an object enters the trigger area
	void OnTriggerEnter(Collider Col)
    {
        // Load the main menu
        SceneManager.LoadScene(0);
    }
}
