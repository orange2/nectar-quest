﻿////////////////////////////////////////////////////////////
// File: MainMenuManager.cs
// Author: Abbas Khan
// Brief: The main menu manager controls
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory
using UnityEngine.SceneManagement;
// Using the UnityEngine  Directory

public class MainMenuManager : MonoBehaviour {
	// Reset Data function
	public void ResetData()
    {
        // Reset all the stored values
		PlayerData.Instance.ResetAllData();
	}
    // Quit game function
    public void QuitGame()
    {
        // Close the application
        Application.Quit();
    }
    // Load Game function
    public void LoadGame()
    {
        // Load the first scene
        SceneManager.LoadScene(1);
    }
}
