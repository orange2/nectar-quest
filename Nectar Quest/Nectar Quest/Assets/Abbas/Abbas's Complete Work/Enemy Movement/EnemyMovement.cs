﻿////////////////////////////////////////////////////////////
// File: EnemyMovement.cs (OLD SXRIPT)
// Author: Abbas Khan
// Brief: Enemy AI System
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine directorys

public class EnemyMovement : MonoBehaviour
{
    // Private navmeshagent Enemy used for the enemy
    private NavMeshAgent Enemy;
    // Boolean to determine whether the player was seen by the enemy
    private bool PlayerSeen = false;
    // Used to reference the player
    public GameObject Player;
    // Vector3 to hold the target point for enemy to walk to
    private Vector3 point;
    // Boolean to determine if the enemy has reached the point or not
    private bool Reached = true;
    // Float for how far the nect point can be
    public float range = 10.0f;

    // If an object hits the enemy
    void OnTriggerEnter(Collider Col)
    {
        // If it's the player
        if(Col.gameObject.tag == "Player")
        { 
            // The player has been seen
            PlayerSeen = true;
        }
        // If it's the player's weapon
        if (Col.gameObject.tag == "PlayerWeapon")
        {
            // The player has been seen
            PlayerSeen = true;
        }
    }
    // Boolean function that takes a vector 3, float and outputs a vector 3
    bool RandomPoint(Vector3 center, float range, out Vector3 result)
    {
        // Generate a random point
        Vector3 randomPoint = center + Random.insideUnitSphere * range;
        // Create a raycast
        NavMeshHit hit;
        // If the raycast hit's the navmesh (Raycast uses random point vector 3)
        if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
        {
            // THe point is valid and is pushed out
            result = hit.position;
            // Return true that the function completed
            return true;
        }
        // Otherwise the raycast didn't hit and the result is not pushed out
        result = Vector3.zero;
        // Return false that the function didn't complete
        return false;
    }

    // Start function which runs at the start of the script's lifetime
    void Start()
    {
        // Get the nav mesh agent from the enemy object
        Enemy = GetComponent<NavMeshAgent>();
    }
    // Update function that is called every frames
    void Update()
    {
        // If the enemy's remaining distance to the target is less than 0.5
        if (Enemy.remainingDistance <= 0.5f)
        {
            // Set reached to true
            Reached = true;
        }
        // Create a raycast hit
        RaycastHit hit;
        // Directions for the target and the ray
        Vector3 rayDirection = Player.transform.position - transform.position;
        Vector3 TargetDirection = transform.position + (transform.forward * 50);
        // Angle is equal to the distance between the target direction and the ray direction
        float Angle = Vector3.Angle(TargetDirection, rayDirection);
        // If the raycast hits anything
        if (Physics.Raycast(transform.position, rayDirection, out hit))
        {
            // If it's position is equal to the player's position, it must be the player
            // and if the angle is less than 20
            if (hit.transform == Player.transform && (Angle < 20))
            {
                // THe player has been seen
                PlayerSeen = true;
            }
            // Otherwise
            else
            {
                // The player wasn't seen
                PlayerSeen = false;
                // Carry on with the current point
                Enemy.SetDestination(point);
            }
        }
        // If the player is seen
        if (PlayerSeen == true)
        {
            // Set the target destiation as the player's position
            Enemy.SetDestination(Player.transform.position);
        }
        // Otherwise
        else
        {
            // If the destination is reached
            if (Reached == true)
            {
                // Create a new randompoint
                if (RandomPoint(transform.position, range, out point))
                {
                    // Set the enemy destination to thr point
                    Enemy.SetDestination(point);
                    // Set reached to false
                    Reached = false;
                }
            }
        }
    }
}