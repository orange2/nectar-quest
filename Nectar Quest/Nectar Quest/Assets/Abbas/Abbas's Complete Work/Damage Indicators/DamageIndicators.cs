﻿////////////////////////////////////////////////////////////
// File: DamageIndicators.cs
// Author: Abbas Khan
// Brief: Used for the in game damage indication
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory
using System.Collections;
// Using the System Collections Directory
using UnityEngine.UI;
// Using the UnityEngine UI Directory

public class DamageIndicators : MonoBehaviour {

    // Public gameobject for the damage indication panel
    public GameObject DamageIndicatorPanel;
    // Public text for the damage indication text
    public Text DamageText;
    // Ienumerator used to flash the damage indication
    IEnumerator DamageFlash()
    {
        // Set the panel to active
        DamageIndicatorPanel.SetActive(true);
        // Wait for a second
        yield return new WaitForSecondsRealtime(1.0f);
        // Set the panel to not active
        DamageIndicatorPanel.SetActive(false);
    }
    // OnTriggerEnter function for when the enemy is hit by the player's weapons
    void OnTriggerEnter(Collider Col)
    {
        // If the object is the player weapon
        if (Col.gameObject.tag == "PlayerWeapon")
        {
            // Set the dmage flash text as 10
            DamageText.text = System.Convert.ToString(10);
            // Start the damage flash coroutine
            StartCoroutine(DamageFlash());
        }
    }
}
