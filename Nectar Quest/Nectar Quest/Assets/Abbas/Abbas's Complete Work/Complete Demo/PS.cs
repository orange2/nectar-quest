﻿////////////////////////////////////////////////////////////
// File: PS.cs (PS = Player Stamina)
// Author: Abbas Khan
// Brief: Used for the player stamina, based off
//        Rebekah's code
////////////////////////////////////////////////////////////

// Using UnityEngine Directory
using UnityEngine;
// Using UnityEngine UI Directory
using UnityEngine.UI;

public class PS : MonoBehaviour
{
    // Added By Abbas
    //////////////////////////////////////////////
    // Animator for player
    static Animator anim;
    // Player's RigidBody
    public Rigidbody PlayerRB;
    // Downward jump force
    private Vector3 Jump = new Vector3(0, -3, 0);
    // Multiplier
    private float JumpForce = 10.0f;
    //////////////////////////////////////////////
    public Image currentHealthbar;
    public Text ratioText;

    public float hitpoint = 200;
    public float maxHitpoint = 200;

    private void Start()
    {
        UpdateHealthbar();
        // Added by Abbas
        ////////////////////////////////////////////////////////////
        // Get the player's animator
        anim = GetComponent<Animator>();
        // Set the max stamina equal to 200 + the upgrade
        maxHitpoint = 200 + PlayerPrefs.GetInt("StaminaUpgrade");
        // Set the current stamina equal to 200 + the upgrade
        hitpoint = 200 + PlayerPrefs.GetInt("StaminaUpgrade");
        ////////////////////////////////////////////////////////////
    }

    private void Update()
    {
        // Set the max stamina equal to 200 + the upgrade Added By Abbas
        maxHitpoint = 200 + PlayerPrefs.GetInt("StaminaUpgrade");

        UpdateHealthbar();
        // Added By Abbas
        ////////////////////////////////////////////////////////////////
        // If the stamina amount is less than 10
        if (hitpoint < 10)
        {
            // Add the downward force
            PlayerRB.AddForce(Jump * (JumpForce), ForceMode.Impulse);
            // Set flying to false
            anim.SetBool("isFlying", false);
        }
        // If flying is true
        if (anim.GetBool("isFlying") == true)
        {
            // Reduce the stamina
            hitpoint -= 50.0f * Time.deltaTime;
        }

        if (hitpoint < 0)
        {
            hitpoint = 0;
        }

        // Regenerate stamina
        HealDamageS(0.5f);
        ////////////////////////////////////////////////////////////////
    }

    private void UpdateHealthbar()
    {
        float ratio = hitpoint / maxHitpoint;
        currentHealthbar.rectTransform.localScale = new Vector3(ratio, 1, 1);
        ratioText.text = (ratio * 100).ToString("0") + '%';
    }

    private void HealDamageS(float heal)
    {
        hitpoint += heal;
        if (hitpoint > 200)
        {
            hitpoint = maxHitpoint;
        }

        UpdateHealthbar();
    }
}