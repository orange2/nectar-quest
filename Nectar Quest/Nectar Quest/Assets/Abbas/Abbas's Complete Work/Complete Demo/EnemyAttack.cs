﻿////////////////////////////////////////////////////////////
// File: EnemyAttack.cs
// Author: Abbas Khan
// Brief: Used for the Enemy Attack (Based off Rebekah's
//        Health System)
////////////////////////////////////////////////////////////

// Using UnityEngine Directory
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    // Bool for damaging
	public bool isDamaging;
    // Float for the amount of damage
    public float damage = 50;
    // If the player enter's the trigger area
	private void OnTriggerEnter(Collider col)
	{
        // If the object is the player
		if (col.tag == "Player")
            // Send a message for damaging the enemy
			col.SendMessage((isDamaging) ? "TakeDamage" : "HealDamage",damage);
	}
	
}
