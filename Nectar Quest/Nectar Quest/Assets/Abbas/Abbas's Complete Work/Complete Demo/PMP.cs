﻿////////////////////////////////////////////////////////////
// File: PMP.cs (PMP = Player Mana Pickup)
// Author: Abbas Khan
// Brief: Used for the mana flower pickup, based off
//        Rebekah's code
////////////////////////////////////////////////////////////

// Using UnityEngine Directory
using UnityEngine;

public class PMP : MonoBehaviour
{
    // Boolean to determine if is healing or not
    public bool isHealing;
    // Heal amount
    public float heal = 5;
    // Amount Healed
    public float healed = 0;
    // If an object enters and stays in the trigger area
    private void OnTriggerStay(Collider col)
    {
        // If it's the player
        if (col.tag == "Player")
        {
            // Send a message for healing of the stamina
            col.SendMessage((isHealing) ? "HealDamageM" : "TakeDamageM", Time.deltaTime * heal);
            // Increase the amount healed
            healed += Time.deltaTime * heal;
        }
        // If the heal - healed amount is less than 0.001
        if ((heal - healed) < 0.001)
        {
            // Turn off the renderer and collider
            gameObject.SetActive(false);
        }
    }
}
