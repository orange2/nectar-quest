﻿////////////////////////////////////////////////////////////
// File: PHMerge.cs
// Author: Abbas Khan
// Brief: Used for the Enemy Attack (Based off Rebekah's
//        Health System)
////////////////////////////////////////////////////////////

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
// Using the UnityEngine scenemanagement Directory Added By Abbas

public class PHMerge : MonoBehaviour
{
    // Public animatoor for the player Added by Abbas
    static Animator anim;

    public Image currentHealthbar;
    public Text ratioText;

    private float hitpoint = 200;
    public float maxHitpoint = 200;

    private void Start()
    {
        UpdateHealthbar();
        // Added by Abbas
        ////////////////////////////////////////////////////////////
        // Get the player's animator
        anim = GetComponent<Animator>();
        // Set the max health equal to 200 + the upgrade
        maxHitpoint = 200 + PlayerPrefs.GetInt("HealthUpgrade");
        // Set the current health equal to 200 + the upgrade
        hitpoint = 200 + PlayerPrefs.GetInt("HealthUpgrade");
        ////////////////////////////////////////////////////////////
    }

    private void Update()
    {
        // Added By Abbas
        ////////////////////////////////////////////////////////////
        // Set the mac health equal to 200 + the upgrade
        maxHitpoint = 200 + PlayerPrefs.GetInt("HealthUpgrade");
        // If the hitpoint <= 0
        if(hitpoint <= 0)
        {
            // Play the death animation
            anim.SetBool("isDead", true);
            // Scene variable that holds the current scene
            Scene CurrentScene = SceneManager.GetActiveScene();
            // Load the current scene again
            SceneManager.LoadScene(CurrentScene.name);

        }
        ////////////////////////////////////////////////////////////
        UpdateHealthbar();
    }

    private void UpdateHealthbar()
    {
        float ratio = hitpoint / maxHitpoint;
        currentHealthbar.rectTransform.localScale = new Vector3(ratio, 1, 1);
        ratioText.text = (ratio * 100).ToString("0") + '%';
    }

    private void TakeDamage(float damage)
    {
        hitpoint -= damage;
        if (hitpoint < 0)
        {
            hitpoint = 0;
            Debug.Log("Dead");
        }

        UpdateHealthbar();
    }

    private void HealDamage(float heal)
    {
        hitpoint += heal;
        if (hitpoint > 200)
        {
            hitpoint = maxHitpoint;
        }

        UpdateHealthbar();
    }
}