﻿// Abbas Khan
// Created for Enemy AI

using UnityEngine;
using System.Collections;
public class Merge2 : MonoBehaviour
{

    public Animator anim; //static variable for animation
    private NavMeshAgent Enemy;
    public bool PlayerSeen = false;
    public GameObject Player;
    private Vector3 point;
    public bool Reached = true;
    public float range = 10.0f;

    public bool Attacking = false;
    public bool noMove = false; //stops movement during animation

    void OnTriggerEnter(Collider Col)
    {
        if(Col.gameObject.tag == "Player")
        { 
            PlayerSeen = true;
        }
        if (Col.gameObject.tag == "PlayerWeapon")
        {
            PlayerSeen = true;
        }
    }

    bool RandomPoint(Vector3 center, float range, out Vector3 result)
    {
        Vector3 randomPoint = center + Random.insideUnitSphere * range;
        NavMeshHit hit;
        if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
        {
            result = hit.position;
            return true;
        }
        result = Vector3.zero;
        return false;
    }

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>(); //get animation component for the character
        Enemy = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (Enemy.remainingDistance >= 0.01f) //if the character's axis movement is more than 0
        {
            anim.SetBool("isWalking", true); //sets bool to true since the character is moving, allows anim
        }
        else
        {
            anim.SetBool("isWalking", false); //changes animation back to idle as the movement is 0
        }

        if(anim.GetBool("isDead") == true)
        {
            noMove = true;
        }

        // print(PlayerSeen);

        if (Enemy.remainingDistance <= 0.5f)
        {
            Reached = true;
        }

        RaycastHit hit;
        Vector3 Offset = new Vector3(0,1,0);
        Vector3 rayDirection = (Player.transform.position + Offset) - (transform.position);

        if (noMove == false && Attacking == false)
        {
            if (Physics.Raycast((transform.position), rayDirection, out hit))
            {
                Debug.DrawRay(transform.position, rayDirection, Color.blue, 1.0f);
                if (hit.transform.gameObject.tag == "Player")
                {
                    PlayerSeen = true;
                }
                else
                {
                    PlayerSeen = false;
                    Attacking = false;
                    noMove = false;
                    Enemy.SetDestination(point);
                }
            }
        }
        if (Attacking == true)
        {
            if (Enemy.remainingDistance < 2.0f)
            {
                noMove = true;
                StartCoroutine(noMoveAttacking());
            }
        }
        if (PlayerSeen == true && noMove == false)
        {
            if (Attacking == false)
            {
                Enemy.SetDestination(Player.transform.position);
            }
            if (Enemy.remainingDistance <= 2.0f)
            {
                Reached = true;
                Attacking = true;
            }
        }
        else
        {
            if (Reached == true && noMove == false)
            {
                if (RandomPoint(transform.position, range, out point))
                {
                    Enemy.SetDestination(point);
                    Reached = false;
                }
            }
        }
        Debug.DrawRay(point, Vector3.up, Color.blue, 1.0f);
    }
    IEnumerator noMoveAttacking()
    {
        anim.SetBool("isAttacking", true); //sets animation to play slash animation
        yield return new WaitForSeconds(1.55f);
        anim.SetBool("isAttacking", false);
        noMove = false;
        Attacking = false;
        Reached = false;
    }
}