﻿////////////////////////////////////////////////////////////
// File: MeleeDamage.cs
// Author: Abbas Khan
// Brief: Used for the Player Attack (Based off Rebekah's
//        Health System)
////////////////////////////////////////////////////////////

// Using the UnityEngine Directory
using UnityEngine;

public class MeleeDamage : MonoBehaviour
{
    // Boolean for damaging
	public bool isDamaging;
    // Damage amount
	public float damage = 50;
    // If the object is in the trigger area
	private void OnTriggerEnter(Collider col)
	{
        // If it's the enemy
		if (col.tag == "Enemy")
            // Send a damage message
            col.SendMessage((isDamaging) ? "TakeDamage" : "HealDamage", damage);
	}
}
