﻿////////////////////////////////////////////////////////////
// File: PM.cs (PM = Player Mana)
// Author: Abbas Khan
// Brief: Used for the player mana, based off
//        Rebekah's code
////////////////////////////////////////////////////////////

// Using UnityEngine Directory
using UnityEngine;
// Using UnityEngine UI Directory
using UnityEngine.UI;

public class PM : MonoBehaviour
{
    // Animator for player Added by Abbas
    static Animator anim;

    public Image currentHealthbar;
    public Text ratioText;

    private float hitpoint = 200;
    public float maxHitpoint = 200;

    // GameObject Ranged Weapon Added By Abbas
    public GameObject RangedWeapon;

    private void Start()
    {
        UpdateHealthbar();
        // Added by Abbas
        ////////////////////////////////////////////////////////////
        // Get the player's animator
        anim = GetComponent<Animator>();
        // Set the max mana equal to 200 + the upgrade
        maxHitpoint = 200 + PlayerPrefs.GetInt("ManaUpgrade");
        // Set the current mana equal to 200 + the upgrade
        hitpoint = 200 + PlayerPrefs.GetInt("ManaUpgrade");
        ////////////////////////////////////////////////////////////
    }

    private void Update()
    {
        // Set the max mana equal to 200 + the upgrade Addeb By Abbas
        maxHitpoint = 200 + PlayerPrefs.GetInt("ManaUpgrade");

        UpdateHealthbar();
        // Added By Abbas
        /////////////////////////////////////////////////////////////
        // If the hitpoint <= 10
        if (hitpoint <= 10)
        {
            // Set the ranged weapon to not active
            RangedWeapon.SetActive(false);
            // Set the attacking magic animation to false
            anim.SetBool("isAttackingMage", false);
        }
        // If the hitpoint is more than 10
        else if(hitpoint >10)
        {
            // Set the ranged weapon to active
            RangedWeapon.SetActive(true);
        }
        // If the attack button is pressed
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            // Take 10 mana from the player
            TakeDamageM(10.0f);
        }
        /////////////////////////////////////////////////////////////
    }

    private void UpdateHealthbar()
    {
        float ratio = hitpoint / maxHitpoint;
        currentHealthbar.rectTransform.localScale = new Vector3(ratio, 1, 1);
        ratioText.text = (ratio * 100).ToString("0") + '%';
    }

    private void TakeDamageM(float damage)
    {
        hitpoint -= damage;
        if (hitpoint < 0)
        {
            hitpoint = 0;
            Debug.Log("Dead");
        }

        UpdateHealthbar();
    }

    private void HealDamageM(float heal)
    {
        hitpoint += heal;
        if (hitpoint > 200)
        {
            hitpoint = maxHitpoint;
        }

        UpdateHealthbar();
    }
}