﻿////////////////////////////////////////////////////////////
// File: EnemyHealthMerge.cs
// Author: Abbas Khan
// Brief: A modified version of Rebekah's code to create an
//        Enemy Health System
////////////////////////////////////////////////////////////

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyHealthMerge : MonoBehaviour
{
    public Image E_currentHealthbar;
    public Text E_ratioText;
    public GameObject EnemyHealthBackground;

    private float hitpoint = 200;
    private float maxHitpoint = 200;

    // Added By Abbas
    ///////////////////////////////////////////////////////
    // Public gameobject for the gem dropped by the enemy
    public GameObject Money;
    // Enemy's Animator
    public Animator anim;
    // WHen something is in the trigger area
    private void OnTriggerEnter(Collider Col)
    {
        // If it's the player or the player weapon
        if (Col.tag == "PlayerWeapon" || Col.tag == "Player")
        {
            // Start that Wait and Show Coroutine 
            StartCoroutine(WaitAndShow());
        }
    }
    ///////////////////////////////////////////////////////

    private void Start()
    {
        // Get the enemy's animator Added By Abbas
        anim = GetComponent<Animator>();

        UpdateHealthbar();
    }

    // Added By Abbas
    ///////////////////////////////////////////////////////
    // Update runs once per frame
    private void Update()
    {
        // If the hitpoints are 0
        if (hitpoint == 0)
        {
            // Start the death animation
            anim.SetBool("isDead", true);
            // Start the wait coroutine
            StartCoroutine(Wait());
        }
    }
    ///////////////////////////////////////////////////////

    private void UpdateHealthbar()
    {
        float ratio = hitpoint / maxHitpoint;
        E_currentHealthbar.rectTransform.localScale = new Vector3(ratio, 1, 1);
        E_ratioText.text = (ratio * 100).ToString("0") + '%';
    }
    private void TakeDamage(float damage)
    {
        hitpoint -= damage;
        if (hitpoint < 0)
        {
            hitpoint = 0;
            Debug.Log("Dead");
        }

        UpdateHealthbar();
    }
    private void HealDamage(float heal)
    {
        hitpoint += heal;
        if (hitpoint > 200)
        {
            hitpoint = maxHitpoint;
        }

        UpdateHealthbar();
    }

    // Added by Abbas
    ///////////////////////////////////////////////////////
    // Coroutine for diaplaying the gem
    IEnumerator Wait()
    {
        // Wait for the animation to finish
        yield return new WaitForSeconds(1.55f);
        // Turn off the Enemy Gameobjcet
        gameObject.SetActive(false);
        // Turn on the gem
        Money.SetActive(true);
    }
    // Coroutine for showing the enemy health bar
    IEnumerator WaitAndShow()
    {
        // Turn on the enemy health bar
        EnemyHealthBackground.SetActive(true);
        // Wait for a while
        yield return new WaitForSeconds(1.55f);
        // Turn off the enemy health bar
        EnemyHealthBackground.SetActive(false);
    }
    ///////////////////////////////////////////////////////
}