﻿////////////////////////////////////////////////////////////
// File: ShootMerge.cs
// Author: Abbas Khan
// Brief: Used for the player's ranged attack
////////////////////////////////////////////////////////////

// Using UnityEngine Directory
using UnityEngine;
// Using System Collections Directory
using System.Collections;

public class ShootMerge : MonoBehaviour {
    // Magic prefab reference
	public GameObject Magic;
    // Player Gameobject reference
    public GameObject Player;
    // Animator for the player
    static Animator anim;

    // Use this for initialization
    void Start()
    {
        // Get the player's animation component
        anim = Player.GetComponent<Animator>();
    }
    // Update called once per frane
    void Update ()	
	{
        // If the attack button is pressed
		if(Input.GetKeyUp(KeyCode.Mouse0))
		{
            // Rotate the player to the camera's direction
            RotateRelativeToCamera(Vector3.forward, Camera.main);
            // Turn on the animation 
            anim.SetBool("isAttackingMage", true);
            // Start the coroutine
            StartCoroutine(Wait());
        }
    }
    // Ienumerator for the ranged attack
    IEnumerator Wait()
    {
        // Wait a while
        yield return new WaitForSeconds(0.2f);
        // Instantiate the prefab
        GameObject m = (GameObject)Instantiate(Magic, transform.position, transform.rotation);
        // Wait a while
        yield return new WaitForSeconds(0.5f);
        // Turn off the animation
        anim.SetBool("isAttackingMage", false);
    }
    // Rotate camera function
    private void RotateRelativeToCamera(Vector3 Direction, Camera Cam)
    {
        // Get the camera's rotation
        Vector3 CamDir = Cam.transform.rotation * Direction;

        // Add the camera's direction to the player's direction
        Vector3 PlayerDir = transform.position + CamDir;

        // Quaternion for direction
        Quaternion targetRotation = Quaternion.LookRotation(PlayerDir - Player.transform.position);

        // Lock the Y-Axis so player doesn't fall over
        Quaternion constrained = Quaternion.Euler(0.0f, targetRotation.eulerAngles.y, 0.0f);

        // Smooth out the rotation
        Player.transform.rotation = Quaternion.Slerp(Player.transform.rotation, constrained, 1);
    }

}