﻿////////////////////////////////////////////////////////////
// File: TextPopup.cs
// Author: Abbas Khan
// Brief: Used for the in game damage indication
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory
using UnityEngine.UI;
// Using the UnityEngine UI Directory

public class TextPopup : MonoBehaviour {

    // Public gameobject for holding the player text object
    public GameObject PopupTextObject;
    // Public sprite for the image that is to be displayed
    public Sprite INPUTIMAGE;
    // Public image for holding the input image on the UI
    public Image Area;
    // OnTriggerEnter - When something hits this object
    void OnTriggerEnter(Collider Col)
    {
        // If that object is the player
        if (Col.gameObject.tag == "Player")
        {
            // Set the area's sprite equal to the inputimage sprite
            Area.sprite = INPUTIMAGE;
            // Display the UI
            PopupTextObject.SetActive(true);
        }
    }
    // OnTriggerExit - When an object leaves the trigger area
    void OnTriggerExit(Collider Col)
    {
        // Hide the UI
        PopupTextObject.SetActive(false);
    }
}
