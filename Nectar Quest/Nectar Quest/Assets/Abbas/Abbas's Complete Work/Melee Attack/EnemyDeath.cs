﻿////////////////////////////////////////////////////////////
// File: EnemyDeath.cs
// Author: Abbas Khan
// Brief: Old Enemy Health System
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory

public class EnemyDeath : MonoBehaviour {

    // If the object is inside the trigger area
    void OnTriggerEnter(Collider Col)
    {
        // If the object is the player's weapon
        if(Col.gameObject.tag == "PlayerWeapon")
        {
            // Destroy the object this script is attatched to
            Destroy(this.gameObject);
        }
    }
}
