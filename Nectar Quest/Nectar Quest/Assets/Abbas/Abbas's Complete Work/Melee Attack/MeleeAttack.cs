﻿////////////////////////////////////////////////////////////
// File: MeleeAttack.cs
// Author: Abbas Khan
// Brief: Melee Attack System
////////////////////////////////////////////////////////////

using UnityEngine;
// Using the UnityEngine Directory
using System.Collections;
// Using the System Collections Directory

public class MeleeAttack : MonoBehaviour {

    // Player's weapon
    public GameObject Weapon;
    // Teh Player
    public GameObject Player;
    // Player's animator
    static Animator anim;
    // Ienumerator used for playing the attack animation and takes in a float wait for the time it needs to wait
    IEnumerator WaitandAttack(float Wait)
    {
        // Wait for the desired amount of time
        yield return new WaitForSeconds(Wait);
        // Playing the animation
        anim.SetBool("isAttackingPhys", false);
    }
	// Start function runs at the start of the script's lifetime
	void Start () {
        // Set the weapon to active
        Weapon.SetActive(true);
        // Get the animator
        anim = GetComponent<Animator>();
    }
	// Update function that is called once per frame
	void Update () {
        // If the melee attack button is pressed
	    if(Input.GetKeyUp(KeyCode.Q))
        {
            // Rotate the player to face the camera
            RotateRelativeToCamera(Vector3.forward, Camera.main);
            // Start playing the animation
            anim.SetBool("isAttackingPhys", true);
            // Start the coroutine
            StartCoroutine(WaitandAttack(1.5f));
        }
	}
    // Rotate player to face same direction as camera function
    private void RotateRelativeToCamera(Vector3 Direction, Camera Cam)
    {
        // Get the camera's rotation
        Vector3 CamDir = Cam.transform.rotation * Direction;

        // Add the camera's direction to the player's direction
        Vector3 PlayerDir = transform.position + CamDir;

        // Quaternion for direction
        Quaternion targetRotation = Quaternion.LookRotation(PlayerDir - Player.transform.position);

        // Lock the Y-Axis so player doesn't fall over
        Quaternion constrained = Quaternion.Euler(0.0f, targetRotation.eulerAngles.y, 0.0f);

        // Smooth out the rotation
        Player.transform.rotation = Quaternion.Slerp(Player.transform.rotation, constrained, 1);
    }

}
