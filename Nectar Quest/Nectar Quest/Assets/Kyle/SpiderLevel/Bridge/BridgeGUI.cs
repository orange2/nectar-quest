﻿// Name: Kyle Tugwell
// Date: 13/05/2017
// Time: 15:38
// Content: Make the bridge fall when player is inside of the collision and Q is pressed.

using UnityEngine;
using System.Collections;

public class BridgeGUI : MonoBehaviour

{
    private bool AllowedToShow; //allows text to show
    public GameObject Text; //allows the GUI to choose a text object

    public GameObject webbridge; //lets you choose the object the text will pop up when collided with
    Animator webBridge; //gets the animation for the bridge

    void Start() //when the scene begins
    {
        AllowedToShow = false;
        webBridge = webbridge.GetComponent<Animator>(); //forces the script to be able to use the bridges animation
    }

    void OnCollisionEnter(Collision col) //when entering the collision
    {
        Text.SetActive(true); //shows text inside of collision around the bridge
        AllowedToShow = true;
    }

    void OnCollisionExit(Collision col) //on exit of collision
    {
        Text.SetActive(false); //gets rid of text outside of collision around the bridge
        AllowedToShow = false;
    }

    void Update() //called every physics step
    {
        if (AllowedToShow == true)
        {
            if (Input.GetKeyDown(KeyCode.Q)) //press Q to break web
            {
                Text.SetActive(false); //gets rid of text when the image is open
                StartCoroutine(destroyWeb()); //delays the object being destroyed
            }
        }
    }

    IEnumerator destroyWeb() //allows delayed events and timings
    {
        yield return new WaitForSeconds(0.5f); //wait 0.5 seconds
        Destroy(gameObject); //destroy the game object holding the bridge
        webBridge.SetBool("isFalling", true); //lets the bridge fall through the animator 
    }
}