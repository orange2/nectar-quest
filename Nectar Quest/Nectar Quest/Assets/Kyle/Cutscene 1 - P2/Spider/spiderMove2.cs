﻿// Name: Kyle Tugwell and Abbas Khan
// Date: 27/04/2017
// Time: 18:04
// Content: Make the spider animation work in the first cutscene.

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class spiderMove2 : MonoBehaviour
{

    public GameObject Fade; // Gameobject reference to the fade image
    public Animator anim; // public reference to the spider animator component

    // Ienumerator for attacking
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(7.0f);
        anim.SetBool("isAttacking", true);
        Fade.SetActive(true); // Set the fade object to active. This means the animation will play.
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("Cutscene1P2"); // Load the next scene, change replace me with the desired scene
    }

    // Use this for initialization
    void Start()
    {
       // anim = GetComponent<Animator>(); //get animation component for the character
        anim.SetBool("isWalking", true); //get that spider walking
        StartCoroutine(Wait());
    }
}