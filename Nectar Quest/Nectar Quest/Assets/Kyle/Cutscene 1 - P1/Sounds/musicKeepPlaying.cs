﻿using UnityEngine;
using System.Collections;

public class musicKeepPlaying : MonoBehaviour
{
    void Start()
    {

    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject); //allows the object to exist when the scene changes
        StartCoroutine(dieMusic()); //delays the object being destroyed
    }

    IEnumerator dieMusic() //allows delayed events and timings
    {
        yield return new WaitForSeconds(12f); //wait 12 seconds
        Destroy(gameObject); //destroy the music object
    }
}