﻿// Name: Kyle Tugwell
// Date: 16/03/2017
// Time: 12:38
// Content: Animation Controller for the Bee Warrior 3D Model.

using UnityEngine;
using System.Collections;

public class beewarrioranim : MonoBehaviour 
{

	static Animator anim; //static variable for animation

    //moving speeds
    public float speed = 1.0F; //sets speed to 1 for default
	public float rotationSpeed = 100.0F; //sets a reasonable rotation speed 
    private bool noMove; //stops movement during animation

	// Use this for initialization
	void Start () {

		anim = GetComponent<Animator> (); //get animation component for the character

    }

	// Update is called once per frame
	void Update () {

        float translation = Input.GetAxis("Vertical") * speed; //sets upward movement
		float rotation = Input.GetAxis ("Horizontal") * rotationSpeed; //detects x-z axis movement
		translation *= Time.deltaTime; //sets translation 
		rotation *= Time.deltaTime; //sets rotation
        // If statement to stop the enemy moving when noMove is true
        if (noMove == false)
        {
            transform.Translate(0, 0, translation); //sets Transformation using previously stated variable
            transform.Rotate(0, rotation, 0); //sets Rotate variable from previous stating 
        }
        //This walking thing should be fine when implementing the AI, as it works on a  translation basis

        if (noMove == true)
        {
            speed = 0; //no move
            rotationSpeed = 0; //no rotate
        }

        if (noMove == false)
        {
            speed = 1;
            rotationSpeed = 100;
        }

        if (translation != 0) //if the character's axis movement is more than 0
		{
			anim.SetBool("isWalking", true); //sets bool to true since the character is moving, allows anim
		}
		else
		{
			anim.SetBool("isWalking", false); //changes animation back to idle as the movement is 0
		}

        //Abbas you will need to make something activate this other than B/N when you implement the AI

        if (Input.GetKeyDown("b"))
        {
            StartCoroutine(noMoveAttacking());
        }

        if (Input.GetKeyDown("n"))
        {
            anim.SetBool("isDead", true);
            noMove = true;
        }
    }

    IEnumerator noMoveAttacking()
    {
        anim.SetBool("isAttacking", true); //sets animation to play slash animation
        noMove = true;
        yield return new WaitForSeconds(1.55f);
        anim.SetBool("isAttacking", false);
        noMove = false;
    }
}
