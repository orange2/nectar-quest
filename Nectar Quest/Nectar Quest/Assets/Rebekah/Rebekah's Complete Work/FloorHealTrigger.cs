﻿using UnityEngine;
using System.Collections;

public class FloorHealTrigger : MonoBehaviour
{
    public bool isHealing;
    public float heal = 5;
    public float healed = 0;

    private void OnTriggerStay(Collider col)
    {
        if (col.tag == "Player")
        {
            col.SendMessage((isHealing) ? "HealDamage" : "TakeDamage", Time.deltaTime * heal);
            healed += Time.deltaTime * heal;
        }

        if ((heal - healed) < 0.001)
        {
            //this.GetComponent<Collider>().enabled = false;
            //this.GetComponent<Renderer>().enabled = false;

            gameObject.SetActive(false); // Added By Abbas To turn off the object
        }
    }

}
