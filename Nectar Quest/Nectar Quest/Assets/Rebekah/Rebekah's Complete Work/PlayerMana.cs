﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerMana : MonoBehaviour
{
    public Image currentManabar;
    public Text ratioText;

    private float manapoint = 100;
    private float maxManapoint = 100;

    private void Update()
    {
        UpdateManabar();

		if (Input.GetKeyDown (KeyCode.Space)) {
			useMagic(5);

		}
    }

    private void UpdateManabar()
    {
        float ratio = manapoint / maxManapoint;
        currentManabar.rectTransform.localScale = new Vector3(ratio, 1, 1);
        ratioText.text = (ratio * 100).ToString("0") + '%';
    }

    private void useMagic(float usedMagicpoint)
    {
        manapoint -= usedMagicpoint;
        if (manapoint < 0)
        {
            manapoint = 0;
            Debug.Log("No Mana left");
        }

        UpdateManabar();
    }

    private void addManapoint(float addMagicpoint)
    {
        manapoint += addMagicpoint;
        if (manapoint > 100)
        {
            manapoint = maxManapoint;
        }

        UpdateManabar();
    }


}