﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyHealth : MonoBehaviour
{
    public Image E_currentHealthbar;
    public Text E_ratioText;

    private float hitpoint = 200;
    private float maxHitpoint = 200;

    private void Start()
    {
        UpdateHealthbar();
    }

    private void UpdateHealthbar()
    {
        float ratio = hitpoint / maxHitpoint;
        E_currentHealthbar.rectTransform.localScale = new Vector3(ratio, 1, 1);
        E_ratioText.text = (ratio * 100).ToString("0") + '%';
    }

    private void TakeDamage(float damage)
    {
        hitpoint -= damage;
        if (hitpoint < 0)
        {
            hitpoint = 0;
            Debug.Log("Dead");
        }

        UpdateHealthbar();
    }

    private void HealDamage(float heal)
    {
        hitpoint += heal;
        if (hitpoint > 200)
        {
            hitpoint = maxHitpoint;
        }

        UpdateHealthbar();
    }
}