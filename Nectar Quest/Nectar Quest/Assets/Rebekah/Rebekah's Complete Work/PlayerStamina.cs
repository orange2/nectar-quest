﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerStamina : MonoBehaviour
{
    public Image currentStaminabar;
    public Text ratioText;

    private float staminapoint = 100;
    private const float maxStaminapoint = 100;
    private const float straminaUseageRate = 0.3f;
    private const float staminaRechargeRate = 0.1f;


    private void Update()
    {
        UpdateStaminabar();
    }

    private void UpdateStaminabar()
    {
		// Put update code here...
		if (Input.GetKey(KeyCode.LeftShift)) {
			// Key is being held so reduce stamina
			staminapoint -= straminaUseageRate;
		} else {
			// Key is not being held so increase stamina
			staminapoint += staminaRechargeRate;
		}

		// Clamp stamina between 0 and 100
		if (staminapoint > maxStaminapoint) {
			staminapoint = maxStaminapoint;
		} else if (staminapoint < 0.0f) {
			staminapoint = 0;	
		}

		// Create the actual stamina bar on screen
        float ratio = staminapoint / maxStaminapoint;
        currentStaminabar.rectTransform.localScale = new Vector3(ratio, 1, 1);
        ratioText.text = (ratio * 100).ToString("0") + '%';
    }
}