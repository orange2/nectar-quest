﻿using UnityEngine;
using System.Collections;

public class FlooraddManaTrigger : MonoBehaviour
{
    public bool isAddingPoint;
    public float addMagicpoint = 5;
    public float addedPoints = 0;

    private void OnTriggerStay(Collider col)
    {
        if (col.tag == "Player")
        {
            col.SendMessage((isAddingPoint) ? "addManapoint" : "useMagic", Time.deltaTime * addMagicpoint);
            addedPoints += Time.deltaTime * addMagicpoint;
        }

        if ((addMagicpoint - addedPoints)  < 0.001)
        {
            this.GetComponent<Collider>().enabled = false;
            this.GetComponent<Renderer>().enabled = false;
        }
    }

}
