﻿using UnityEngine;
using System.Collections;

public class FloorManaTrigger : MonoBehaviour
{
    public bool isUsingMagic;
    public float usedMagicpoint = 10;

    private void OnTriggerStay(Collider col)
    {
        if (col.tag == "Player")
            col.SendMessage((isUsingMagic) ? "useMagic" : "addManapoint", Time.deltaTime * usedMagicpoint);
    }

}