﻿//
//                          How to use docuement
//
// Abbas: 
//
// I've Created a folder for each of you to store any bits of work that you
// create for the unity project. Whenever you are working on a new element for the
// game duplicate the scene that you intend to put the element into or create a new
// scene for that element. Once you are happy with the element and it's complete, 
// copy the element into the origonal scene in the nectar quest folder.
//
// Example:
// I want to create a new mechanic,
// I create a new scene so i can test the mechanic.
// Once i complete it, i copy the mechanic into the main game.
//
// Do not under any circumstances work directly from the scenes in the nectar quest
// folder. 
//
// Ensure to put your individual bits of work in your own folders, if it is 
// something multiple people have worked on put it into the shared folder.
// 
// if you've got any questions, about how to do anything in the unity project
// ask in the facebook group chat.
//
// Cheers
// Abbas