﻿//
//                  Coding Instructions
//
// Abbas:
// 
// Below are a couple of rules that I suggest you follow. Otherwise the project
// may break and/or be annoying for others to code with.
//
// 1. Comment all variables and functions
// 
// This way anybody who needs to build on other people's code know what's going on.
// 
// 2. Use appropriate access levels
// 
// Things like the amount of health and such should be private. Where as 
// functions can sometimes be public depending on the purpose. Just be smart about it.
// 
// 3. The Coding Standards Docuement found within this project should be followed
//
// When assigning names to anything in code, follow the docuement.
// The docuement does not cover unity specific code. So, when using these capitalise
// the first letter of the item. If it has multiple parts to the name add underscores.
//
// Example for normal variable
// Eg. private int i_Health
//
// Example for unity variable
// eg. public GameObject g_o_Player
//
// Cheers
// Abbas
