﻿//
//              BitBucket Usage Instructions
//
// Abbas:
//
// Now that I've shown you guys how to use sourcetree & bitbucket,
// this is somewhat irrelevant but I'll keep it here just in case.
//
//
//              Initial Download Instructions
//
// In order to get the project, you'll need to download sourcetree.
//
// Once downloaded login and then in chrome navigate to bitbucket
// website. 
//
// Logon and navigate to the project repository. 
//
// Get the clone url from the top right of the page.
//
// Back in source tree hit clone and paste the url in the top text field.
//
// Allow it to auto fill the settings and change the destination path to
// a folder on your computer's desktop. (Create one if you haven't already)
// 
// Hit clone and you should be good, it'll download into the folder on 
// the desktop.
//
//
//                    Push & Pull Instructions
//
// After you've finished working in unity you need to push up to the 
// online repository before anyone can see your work.
//
// In order to push you need save your work and close any open files
// realted to the project.
//
// Navigate to source tree and stage all of the new changes.
//
// Add a comment in the textfield at the bottom & hit commit.
//
// When you add the comment it should be in the below format to keep consistency.
//
// Example (Ignore the '//' )
//
// Name: Abbas Khan
// Date: 15/03/2017
// Time: 17:58
// Content: Cleaned up project folders & updated instructions docuements.
//
// Hit push to upload it to the online repository.
//
// If it fails, the most likely problem is your project is out of date. 
// Hit pull to get the latest version and then push it. 
//
// Below is a reference video I used to help setup the project for bitbucket
// if any of you are intrested. 
//
// Reference Video: https://www.youtube.com/watch?v=yXY3xidmQm0
//
// Cheers
// Abbas