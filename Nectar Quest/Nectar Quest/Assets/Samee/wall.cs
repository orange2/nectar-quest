﻿using UnityEngine;

public class wall : MonoBehaviour {

    public GameObject Wall;

    void OnTriggerEnter(Collider Col)
    {
        // If that object is the player
        if (Col.gameObject.tag == "Player")
        {
            Wall.SetActive(true);
        }
    }

}
